import Messages from "./Messages";
import { IMessage } from "./Message";
import SendMessage from "./SendMessages";
import { Socket } from "socket.io-client";
import { useState } from "react";

interface Props {
  messages: IMessage[];
  socket: Socket;
  username: string;
}

const Chat = ({ messages, socket, username }: Props) => {
  const [selectedLanguage, setSelectedLanguage] = useState("1");

  const handleLanguageChange = (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => {
    setSelectedLanguage(event.target.value);
  };
  return (
    <div className="bg-white w-11/12 mx-auto rounded-lg shadow-md mt-20 mb-20">
      <p> Langage de traduction </p>
      <div>
        <select
          data-te-select-init
          data-te-select-filter="true"
          value={selectedLanguage}
          onChange={handleLanguageChange}
        >
          <option value="1">Français</option>
          <option value="2">Anglais</option>
          <option value="3">Espagnol</option>
          <option value="4">Allemand</option>
          <option value="5">Chinois</option>
          <option value="6">Russe</option>
          <option value="7">Arabe</option>
        </select>
      </div>
      {/* Composant pour afficher les messages */}
      <Messages messages={messages} username={username} />
      <br></br>

      {/* Composant pour envoyer des messages */}
      <SendMessage socket={socket} username={username} />
    </div>
  );
};

export default Chat;
