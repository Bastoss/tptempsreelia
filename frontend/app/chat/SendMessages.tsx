import { useState } from "react";
import { Socket } from "socket.io-client";
import Input from '@mui/joy/Input';
import Button from '@mui/joy/Button';
import Snackbar from '@mui/joy/Snackbar';
import Typography from '@mui/joy/Typography';



export interface Props {
  socket: Socket;
  username: string;
}

const SendMessage = ({ socket, username }: Props) => {
  const [text, setText] = useState("");
  const [open, setOpen] = useState(false);

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    console.log(username)
    console.log(text.trim())

    // Vérifier si un nom d'utilisateur est défini et si le texte du message n'est pas vide
    if (username && text.trim() !== "") {
      socket.emit("chat-message", {
        content: text,
        timeSent: new Date().toISOString(),
        username: username, // Envoyer le nom d'utilisateur avec le message
      });
      setText("");
    }
    else{
      setOpen(true);
    }
  };

  return (
    <div className="rounded-lg p-5">
      <form className="flex gap-3 flex-end w-12/12" onSubmit={handleSubmit}>
        <Input value={text}  onChange={(e) => setText(e.target.value)} className="w-10/12" color="primary" placeholder="Écrire un message..." />
        <Button className="bg-blue-700/100 w-2/12"
          color="primary"
          size="md"
          variant="solid"
          type="submit"
        >Envoyer
        </Button>
      </form>
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'center'}}
        open={open}
        onClose={() => setOpen(false)}
        color="danger"
        variant="soft"
      >
        <div>
        <Typography color="danger" level="title-lg">Erreur</Typography>
        <Typography color="danger">Veuillez entrer un nom d&apos;utilisateur avant d&apos;envoyer un message.</Typography>
        </div>
        
      </Snackbar>
    </div>
  );
};

export default SendMessage;
