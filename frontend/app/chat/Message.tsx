import { MouseEvent, useEffect, useState } from "react";
import { io } from "socket.io-client";

//Permet de creer un type de type Imessage
export interface IMessage {
  content: string;
  timeSent: string;
  username: string;
  isMe: boolean;
  index: number;
}

const socket = io("http://localhost:3001");

const Message = ({ username, isMe, content, index }: IMessage) => {
  const [translatedContent, setTranslatedContent] = useState<string | null>(
    null
  );

  const [verifiedContent, setVerifiedContent] = useState<string | null>(null);

  const [proposedContent, setProposedContent] = useState<string | null>(null);

  useEffect(() => {
    socket.on("translation", (translatedContent) => {
      console.log(translatedContent);

      if (translatedContent.id === index)
        setTranslatedContent(translatedContent.translate);
    });

    socket.on("verified", (verifiedContent) => {
      console.log(verifiedContent);

      if (verifiedContent.id === index)
        setVerifiedContent(verifiedContent.verifier);
    });

    socket.on("proposed", (proposedContent) => {
      console.log(proposedContent);

      if (proposedContent.id === index)
        setProposedContent(proposedContent.proposer);
    });

    return () => {
      socket.off("translation");
      socket.off("verified");
      socket.off("proposed");
    };
  }, []);

  const translate = () => {
    console.log("On envoie ce contenu: ", content);
    socket.emit("translate", { content, targetLanguage: "anglais", index });
  };

  const verifier = () => {
    console.log("On envoie ce contenu: ", content);
    socket.emit("verifier", index);
  };

  const proposer = () => {
    console.log("On envoie ce contenu: ", content);
    socket.emit("proposer", index);
  };

  return (
    <div className={isMe ? "chat chat-end" : "chat chat-start"}>
      <div className="chat-header flex">{isMe ? "Moi" : username}</div>
      <div
        className={
          isMe
            ? "chat-bubble chat-bubble-info bg-blue-600 text-white"
            : "chat-bubble chat-bubble-info bg-slate-200"
        }
      >
        {translatedContent || content}
        {verifiedContent}
        {proposedContent}
      </div>
      <div className="chat-footer flex gap-1">
        <p className="opacity-40">Envoyé - </p>
        <p
          onClick={translate}
          className="text-blue-500 opacity-100 cursor-pointer underline"
        >
          Traduire
        </p>
        <p
          onClick={verifier}
          className="text-blue-500 opacity-100 cursor-pointer underline"
        >
          Vérifier
        </p>
        <p
          onClick={proposer}
          className="text-blue-500 opacity-100 cursor-pointer underline"
        >
          Proposer un réponse
        </p>
      </div>
    </div>
  );
};

export default Message;
