"use client";
import { useEffect, useState } from "react";
import { io } from "socket.io-client";
import Messages from "./Messages";
import SendMessage from "./SendMessages";
import ConteneurMessages from "./ConteneurMessages";
import Username from "./Username";

const socket = io("http://localhost:3001");

const Chat = () => {
  const [messages, setMessages] = useState([]);
  const [username, setUsername] = useState(""); // État pour stocker le nom d'utilisateur
  const [isUsernameTaken, setIsUsernameTaken] = useState(false); // État pour indiquer si le nom d'utilisateur est pris

  useEffect(() => {
    socket.on("connect", () => {
      console.log("connected");
    });

    socket.on("chat-message", (data) => {
      setMessages((msg) => [...msg, data] as any);
    });

    // Écouter l'événement côté client pour indiquer si le nom d'utilisateur est pris
    socket.on("username-taken", (isTaken) => {
      setIsUsernameTaken(isTaken);
    });
  }, []);

  // Fonction pour définir le nom d'utilisateur localement
  const setUsernameHandler = (newUsername: string) => {
    // Vérifier si le nom d'utilisateur est déjà pris avant de le définir
    if (!isUsernameTaken) {
      setUsername(newUsername);
    }
  };

  return (
    <div>
      <div className="flex flex-col items-center justify-center pt-20 mb-10">
        <h1 className="text-5xl">Bienvenue sur notre chat en temps réel</h1>
        <p className="mt-5">
          Il y a des fonctionnalités d&apos;IA que vous pourrez découvrir !
        </p>
        {/* Afficher le nom d'utilisateur actuel s'il n'est pas pris */}
        {!isUsernameTaken && (
          <p className="text-gray-400">
            <i>Connecté(e) en tant que {username}</i>
          </p>
        )}
      </div>

      {/* Composant pour saisir le nom d'utilisateur */}
      <Username socket={socket} setUsername={setUsernameHandler} />

      <ConteneurMessages
        messages={messages}
        socket={socket}
        username={username}
      />
    </div>
  );
};

export default Chat;
