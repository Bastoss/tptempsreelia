import Message, {IMessage} from "./Message"


interface Props{
    messages: IMessage[];
    username:string;
}





const Messages = ({messages,username}: Props) => {
    return (
        <div>
            <div className="mx-10 py-5">
                {messages.map((msg,index) => (
                    <div key={msg.timeSent}>
                        <Message index={index} isMe={msg.username === username} username={msg.username} content={msg.content} timeSent={msg.timeSent}/>
                    </div>
                ))}
            </div>
        </div>
    );

}


export default Messages;