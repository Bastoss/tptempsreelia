import { useState, useEffect } from "react";
import { Socket } from "socket.io-client";
import Input from '@mui/joy/Input';
import Button from '@mui/joy/Button';




export interface Props {
  socket: Socket;
  setUsername: (username: string) => void;
}

const Username = ({ socket, setUsername }: Props) => {
  const [text, setText] = useState("");
  const [etat, setEtat] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    socket.emit("set-username", text);

    socket.on("username-error", (error) => {
      setErrorMessage(error);
    });

    if (!errorMessage) {
      setUsername(text);
      setEtat(true);
    }
  };

  useEffect(() => {
    return () => {
      setErrorMessage("");
    };
  }, []);

  return (
    <div className="flex flex-col justify-center items-center">
      <form className="flex gap-4" onSubmit={handleSubmit}>
        <Input disabled={etat} value={text}  onChange={(e) => setText(e.target.value)} className="w-12/12" color="primary" placeholder="Nom d'utilisateur" />
        <Button className="bg-blue-700/100"
          color="primary"
          size="md"
          variant="solid"
          type="submit"
        >Enregistrer</Button>
        {/* <button >Enregistrer</button> */}
        {errorMessage && <p style={{ color: "red" }}>{errorMessage}</p>}
    </form>
    </div>
    
    
  );
};

export default Username;
