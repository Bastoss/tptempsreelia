class User {
  constructor(
    public id: string,
    public username: string,
  ) {}
}

import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import OpenAI from 'openai';
import { Socket } from 'socket.io';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule.forRoot()],
})
@WebSocketGateway({ cors: true })
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Socket;

  private users: User[] = [];
  private openai: OpenAI;

  constructor() {
    this.openai = new OpenAI({
      apiKey: process.env.OPENAI_API_KEY,
    });
  }

  @SubscribeMessage('chat-message')
  handleMessage(client: any, payload: any): string {
    this.server.emit('chat-message', payload);
    return 'Hello world!';
  }

  handleConnection(client: any) {
    console.log('client connected', client.id);
    const newUser = new User(client.id, 'Utilisateur par défaut');
    this.users.push(newUser);
  }

  handleDisconnect(client: any) {
    console.log('client disconnected', client.id);
    const index = this.users.findIndex((user) => user.id === client.id);
    if (index !== -1) {
      this.users.splice(index, 1);
    }
  }

  @SubscribeMessage('set-username')
  setUsername(client: any, newUsername: string) {
    const user = this.users.find((user) => user.id === client.id);

    // Vérifier si le nom d'utilisateur existe déjà
    if (!this.isUsernameTaken(newUsername)) {
      user.username = newUsername;
      client.emit('username-set', newUsername); // Émettre un événement indiquant que le nom d'utilisateur a été défini
    } else {
      // Émettre un message d'erreur
      client.emit('username-error', "Ce nom d'utilisateur est déjà pris.");
    }
  }

  // Méthode pour vérifier si un nom d'utilisateur est déjà pris
  private isUsernameTaken(username: string): boolean {
    return this.users.some((user) => user.username === username);
  }

  @SubscribeMessage('translate')
  async handleTranslate(
    client: any,
    payload: { content: string; targetLanguage: string; index: string },
  ) {
    const { content, targetLanguage, index } = payload;

    const response = await this.openai.chat.completions.create({
      model: 'gpt-3.5-turbo',
      messages: [
        {
          role: 'system',
          content: `Donne moi uniquement la traduction du texte qui va suivre en ${targetLanguage}. Je ne veux pas que tu rajoutes un commentaire, ni que tu te mette à y réponde comme si c'était une question que l'on te posait. Contente toi de TOUT traduire.`,
        },
        { role: 'user', content },
      ],
      max_tokens: 150,
      stop: ['\n'],
      temperature: 0.5,
    });

    const translatedContent = {
      translate: response.choices[0].message.content,
      id: index,
    };

    this.server.emit('translation', translatedContent);
  }

  @SubscribeMessage('verifier')
  async handleVerifier(
    client: any,
    payload: { content: string; index: string },
  ) {
    const { content, index } = payload;

    const response = await this.openai.chat.completions.create({
      model: 'gpt-3.5-turbo',
      messages: [
        {
          role: 'system',
          content: `Vérifie l'info suivante, dis moi si elle est vraie ou non, et pourquoi.`,
        },
        { role: 'user', content },
      ],
      max_tokens: 150,
      stop: ['\n'],
      temperature: 0.5,
    });

    const VerifiedContent = {
      translate: response.choices[0].message.content,
      id: index,
    };

    this.server.emit('verified', VerifiedContent);
  }

  @SubscribeMessage('proposer')
  async handleProposer(
    client: any,
    payload: { content: string; index: string },
  ) {
    const { content, index } = payload;

    const response = await this.openai.chat.completions.create({
      model: 'gpt-3.5-turbo',
      messages: [
        {
          role: 'system',
          content: `Propose moi une réponse au message qui va suivre.`,
        },
        { role: 'user', content },
      ],
      max_tokens: 150,
      stop: ['\n'],
      temperature: 0.5,
    });

    const proposedContent = {
      translate: response.choices[0].message.content,
      id: index,
    };

    this.server.emit('proposed', proposedContent);
  }
}
